package alien.taskQueue.jobsplit;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alien.taskQueue.JDL;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class SplitProduction extends JobSplitter {

	/**
	 * @param strategy
	 */
	SplitProduction(final String strategy) {
		this.strategy = strategy;
	}

	@Override
	List<JDL> splitJobs(final JDL j, final long masterId) throws IllegalArgumentException {
		final String pattern = "^production:(.+)-(.+)";
		final Pattern r = Pattern.compile(pattern);
		final int fSize = 0;

		final Matcher m = r.matcher(strategy);

		int start = 0;
		int end = 0;
		try {
			m.matches();
			start = Integer.parseInt(m.group(1));
			end = Integer.parseInt(m.group(2));
			counter = start;
		}
		catch (final Exception e) {
			throw new IllegalArgumentException("Error splitting production: " + e.getMessage());
		}

		final JDL baseJDL = prepareSubJobJDL(j, masterId);
		baseJDL.set("Requirements", setSizeRequirements(fSize));
		final List<String> inputData = j.getInputData();
		final List<JDL> jdls = new ArrayList<>();
		for (int i = start; i <= end; i++) {
			final JDL tmpJdl = changeFilePattern(baseJDL, inputData);
			jdls.add(tmpJdl);
		}

		return jdls;

	}

}
