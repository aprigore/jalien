package alien.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author costing
 * @since 2025-01-20
 */
public class EntryPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try (PrintWriter pw = response.getWriter()) {
			pw.println("You are talking to JAliEn");
		}
	}
}
