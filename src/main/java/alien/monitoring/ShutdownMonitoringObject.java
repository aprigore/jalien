package alien.monitoring;

import java.util.Vector;

/**
 * @author costing
 * @since Jan 22, 2025
 */
public interface ShutdownMonitoringObject extends MonitoringObject {

	/**
	 * Fill the two vectors with names and values produced by the instance. This is the last call for a monitoring object, so send zeroes for example to clean up ML caches.
	 *
	 * @param paramNames
	 * @param paramValues
	 */
	void fillShutdownValues(final Vector<String> paramNames, final Vector<Object> paramValues);
}
