package alien.io.xrootd.envelopes;

import alien.catalogue.LFN_CSD;
import alien.catalogue.access.XrootDEnvelope;

import java.security.GeneralSecurityException;

/**
 * @author Vova
 * @since May 2024
 */
public abstract class AuthzToken {

    /**
     * Initializes the plain envelope
     * @param envelope 
     * @param lfnc 
     */
    public abstract void init(final XrootDEnvelope envelope, final LFN_CSD lfnc);

    /**
     * A method to seal the XrootDEnvelope by encrypting or signing it
     *
     * @param envelope the XrootDEnvelope to be signed or encrypted
     * @return the sealed XrootDEnvelope
     * @throws GeneralSecurityException 
     */
    public abstract String seal(final XrootDEnvelope envelope) throws GeneralSecurityException;

    /**
     * A method to unseal the XrootDEnvelope by decrypting it
     *
     * @param rawToken the encrypted XrootDEnvelope
     * @return the plain XrootDEnvelope
     * @throws GeneralSecurityException 
     */
    public abstract String unseal(final String rawToken) throws GeneralSecurityException;
}
