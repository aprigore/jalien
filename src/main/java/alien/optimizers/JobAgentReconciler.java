/**
 *
 */
package alien.optimizers;

import java.io.IOException;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.config.ConfigUtils;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.taskQueue.JDL;
import alien.taskQueue.JobStatus;
import alien.taskQueue.TaskQueueUtils;
import lazyj.DBFunctions;

/**
 * @author costing
 * @since May 7, 2024
 */
public class JobAgentReconciler extends Optimizer {
	@SuppressWarnings("hiding")
	static final Logger logger = ConfigUtils.getLogger(JobAgentReconciler.class.getCanonicalName());

	static final Monitor monitor = MonitorFactory.getMonitor(JobAgentReconciler.class.getCanonicalName());

	@Override
	public void run() {
		this.setSleepPeriod(Duration.ofMinutes(30).toMillis());
		final int frequency = (int) this.getSleepPeriod();

		while (true) {
			logger.log(Level.INFO, "JobAgentReconciler starting");
			final boolean updated = DBSyncUtils.updatePeriodic(frequency, JobAgentReconciler.class.getCanonicalName(), this);
			try {
				if (updated) {
					try (Timing t = new Timing(monitor, "JAReconciler_ms")) {
						final StringBuilder registerLog = new StringBuilder();
						jaCounterUpdate(registerLog);
						missingJAEntries(registerLog);

						DBSyncUtils.registerLog(JobAgentReconciler.class.getCanonicalName(), registerLog.toString());
					}
				}
			}
			catch (final Exception e) {
				try {
					logger.log(Level.SEVERE, "Exception executing optimizer", e);
					DBSyncUtils.registerException(JobAgentReconciler.class.getCanonicalName(), e);
				}
				catch (final Exception e2) {
					logger.log(Level.SEVERE, "Cannot register exception in the database", e2);
				}
			}

			try {
				logger.log(Level.INFO, "JobAgentReconciler sleeps " + this.getSleepPeriod() + " ms");
				sleep(this.getSleepPeriod());
			}
			catch (final InterruptedException e) {
				logger.log(Level.WARNING, "JobAgentReconciler interrupted", e);
				return;
			}
		}
	}

	/**
	 * @param registerLog
	 */
	private static void missingJAEntries(final StringBuilder registerLog) {
		try (DBFunctions db = getDB(); Timing t = new Timing()) {
			db.setQueryTimeout(60);

			db.query("SELECT queueId FROM QUEUE LEFT OUTER JOIN JOBAGENT ON entryId=agentId WHERE statusId=" + JobStatus.WAITING.getAliEnLevel() + " AND entryId IS NULL;");

			final Set<Long> jobIDs = new HashSet<>();

			while (db.moveNext()) {
				jobIDs.add(Long.valueOf(db.getl(1)));
			}

			registerLog.append("Jobs with a missing JOBAGENT entry: ").append(jobIDs.size()).append(" (").append(t).append(")\n");

			t.startTiming();

			final Set<Integer> agentIDs = new HashSet<>();

			for (final Long queueId : jobIDs) {
				try {
					final JDL jdl = new JDL(queueId.longValue());

					final int agentId = TaskQueueUtils.updateOrInsertJobAgent(queueId.longValue(), jdl);

					if (agentId > 0)
						db.query("UPDATE QUEUE SET agentId=? WHERE queueId=?;", false, Integer.valueOf(agentId), queueId);
					else {
						logger.log(Level.WARNING, "Could not figure out an agent ID for this job: " + queueId);
						registerLog.append("Could not figure out an agent ID for this job: ").append(queueId).append("\n");
					}

					agentIDs.add(Integer.valueOf(agentId));

					if (logger.isLoggable(Level.FINE))
						logger.log(Level.FINE, queueId + " updated JOBAGENT to " + agentId);
				}
				catch (final IOException e) {
					logger.log(Level.WARNING, "Could not load the JDL of " + queueId, e);
				}
			}

			registerLog.append("Inserted ").append(agentIDs.size()).append(" agents that were missing (").append(t).append(")\n");
		}
	}

	private static DBFunctions getDB() {
		return ConfigUtils.getDB("processes");
	}

	/**
	 *
	 */
	private static void jaCounterUpdate(final StringBuilder registerLog) {
		try (DBFunctions db = getDB(); Timing t = new Timing()) {
			db.setQueryTimeout(60);

			db.query("UPDATE JOBAGENT LEFT OUTER JOIN (SELECT agentId, count(1) as cnt from QUEUE where statusId=" + JobStatus.WAITING.getAliEnLevel()
					+ " group by 1) q ON agentId=entryId SET counter=coalesce(cnt,0) where counter!=coalesce(cnt, 0);");
			registerLog.append("JOBAGENT rows with different counter than actual waiting jobs: ").append(db.getUpdateCount()).append(" (").append(t).append(")\n");

			t.startTiming();

			db.query("DELETE FROM JOBAGENT WHERE counter=0;");
			registerLog.append("JOBAGENT rows that had zero jobs waiting and were deleted: ").append(db.getUpdateCount()).append(" (").append(t).append(")\n");
		}
	}
}
