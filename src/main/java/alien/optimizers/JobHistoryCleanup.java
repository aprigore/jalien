package alien.optimizers;

import java.util.logging.Level;

import alien.config.ConfigUtils;
import alien.monitoring.Timing;
import lazyj.DBFunctions;

/**
 * @author Elena Mihailescu
 * @since 2024-08-20
 */
public class JobHistoryCleanup extends Optimizer {

	private static final Integer createdThreshold = Integer.valueOf(14); // days
	private static final Integer lastUpdatedTreshold = Integer.valueOf(7); // days
	private String historyTable = "TTLPredictionHistorySite";

	/**
	 * Default constructor
	 */
	public JobHistoryCleanup() {
		super.setSleepPeriod(60 * 5000L); // 5 min
	}

	/**
	 * @param historyTable
	 */
	public JobHistoryCleanup(final String historyTable) {
		this();
		this.historyTable = historyTable;
	}

	private void cleanupTable() {
		logger.log(Level.FINER, "Starting job history cleanup iteration");

		try (DBFunctions db = ConfigUtils.getDB("processes")) {
			if (db == null) {
				logger.log(Level.WARNING, "Job History Cleanup could not get a DB!");
				return;
			}

			db.setQueryTimeout(60);

			try (Timing t = new Timing()) {
				db.query("DELETE FROM " + historyTable
						+ " WHERE `created` < date_add(now(), INTERVAL -? DAY)"
						+ " OR lastUpdated < date_add(now(), INTERVAL -? DAY);",
						false,
						createdThreshold,
						lastUpdatedTreshold);

				final String log = "Deleted " + db.getUpdateCount() + " from " + historyTable + " in " + t;

				DBSyncUtils.registerLog(this.getClass().getCanonicalName(), log);
				logger.log(Level.FINER, log);
			}

		}
		catch (final Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void run() {
		logger.log(Level.INFO, "Starting Job History Cleanup thread");

		while (true) {
			final int frequency = (int) this.getSleepPeriod();
			try {
				final boolean updated = DBSyncUtils.updatePeriodic(frequency, JobHistoryCleanup.class.getCanonicalName(), this);
				if (updated) {
					cleanupTable();
				}
			}
			catch (final Exception e) {
				try {
					logger.log(Level.SEVERE, "Exception executing JobHistoryCleanup optimizer", e);
					DBSyncUtils.registerException(JobHistoryCleanup.class.getCanonicalName(), e);
				}
				catch (final Exception e2) {
					logger.log(Level.SEVERE, "Cannot register exception in the database", e2);
				}
			}

			try {
				logger.log(Level.INFO, "JobHistoryCleanup sleeping for " + frequency + " ms");
				sleep(frequency);
			}
			catch (final InterruptedException e) {
				logger.log(Level.SEVERE, "JobAgentUpdater interrupted", e);
			}
		}

	}
}
