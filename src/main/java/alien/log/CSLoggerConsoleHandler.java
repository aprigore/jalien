package alien.log;

import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.ConsoleHandler;
import java.util.logging.LogRecord;
import alien.api.Request;
import alien.api.taskQueue.SendRemoteLog;
import alien.api.taskQueue.TaskQueueApiUtils;
import alien.config.ConfigUtils;

/**
 * @author marta
 */
public class CSLoggerConsoleHandler extends ConsoleHandler {

	static boolean debugFlag = false;
	static boolean initFlag = true;
	static LinkedBlockingQueue<LogRecord> queuedLogs;

	/**
	 * Creates a console handler through which we send logs to the cs
	 *
	 */
	public CSLoggerConsoleHandler() {
		super();
		String jobId = System.getProperties().getProperty("jobagent.vmid");
		if (jobId != null) {
			initFlag = false;
			debugFlag = false;
			String runnerId = System.getProperties().getProperty("JobRunnerUUID");
			if (runnerId != null)
				debugFlag = true;
		}
		queuedLogs = new LinkedBlockingQueue<>(1000);
		final Thread logReporter = new Thread(logReporter(), "CS remote log sender");
		logReporter.start();
	}

	@Override
	public synchronized void publish(LogRecord record) {
		super.publish(record);

		if (initFlag || debugFlag) {
			if (record.getThrown() != null) {
				String exceptionSource = Arrays.toString(record.getThrown().getStackTrace());

				if (exceptionSource.contains(CSLoggerConsoleHandler.class.getCanonicalName())) {
					System.out.println("Logging exception " + exceptionSource);
					System.out.println("Skipping because I'm in the throwable somewhere:\n" + exceptionSource);
					return;
				}
			}

			StackTraceElement[] currentStack = Thread.currentThread().getStackTrace();

			int start = 0;

			//First we find the first instance of CSLogger in the stack
			for (; start < currentStack.length; start++)
				if (currentStack[start].getClassName().contains("CSLoggerConsoleHandler"))
					break;

			//Then we skipp all consecutive instances of CSLogger
			for (; start < currentStack.length; start++)
				if (!currentStack[start].getClassName().contains("CSLoggerConsoleHandler"))
					break;

			//Lastly we see if it is a loop --> we get back to CSLogger
			for (int i = start; i < currentStack.length; i++) {
				if (currentStack[i].getClassName().contains("CSLoggerConsoleHandler")) {
					System.out.println(System.currentTimeMillis() + " Skipping because I'm in the stack trace at position " + i + " : " + currentStack[i]);
					return;
				}
			}

			try {
				queuedLogs.offer(record);
			}
			catch (Exception e) {
				// Just not insert that message
				System.out.println("Exception on offering message " + e.getMessage());
			}
		}
	}

	/**
	 *
	 * Iterates over the logs and sends them centrally
	 *
	 */
	@SuppressWarnings("unused")
	private static final Runnable logReporter() {
		return () -> {
			System.out.println("Starting remote log reporter thread");
			try {
				Thread.sleep(10 * 1000);
			}
			catch (InterruptedException e1) {
				System.out.println("Sleep was interrupting. Exiting CSLoggerConsoleHandler thread");
				return;
			}
			if (debugFlag == false) {
				debugFlag = ConfigUtils.getDebugFlag();
				initFlag = false;
			}

			System.out.println("Remote debugging set to " + debugFlag);
			while (debugFlag) {
				try {
					LogRecord record = queuedLogs.take();
					String vmUID = Request.getVMID().toString();
					String runnerId = System.getProperties().getProperty("JobRunnerUUID");
					if (runnerId == null) {
						runnerId = Request.getVMID().toString();
					}
					String host = ConfigUtils.getLocalHostname();
					String site = System.getenv().get("CE");
					SendRemoteLog log = new SendRemoteLog(site, host, runnerId, vmUID, record);
					TaskQueueApiUtils.sendRemoteLog(log);
				}
				catch (InterruptedException e) {
					System.out.println("CSLoggerConsoleHandler was interrupted. Exiting thread");
					return;
				}
			}
		};
	}

}
