package alien.api.taskQueue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import alien.api.Request;
import alien.taskQueue.TaskQueueUtils;

/**
 * Inspect the JA variables for the site specifics
 *
 * @author marta
 * @since
 */
public class GetJAVariables extends Request {
	/**
	 *
	 */
	private static final long serialVersionUID = 5022083696413315512L;

	private String siteName;
	private HashMap <String,String> jaVariables;

	/**
	 * @param siteName
	 */
	public GetJAVariables(final String siteName) {
		this.siteName = siteName;
	}

	@Override
	public List<String> getArguments() {
		return Arrays.asList(siteName);
	}

	@Override
	public void run() {
		jaVariables = TaskQueueUtils.getJAVariables(siteName);
	}

	/**
	 * @return tunnedVariables
	 */
	public HashMap<String,String> getTunnedVariables() {
		return this.jaVariables;
	}

	@Override
	public String toString() {
		String fullVariableList = "";
		for (String key : jaVariables.keySet()) {
			fullVariableList += key + ": " + jaVariables.get(key).toString() + "\n";
		}
		return fullVariableList;
	}
}
