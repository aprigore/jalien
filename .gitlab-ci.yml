stages:
  - unit
  - analysis
  - e2e
  - integration_test
  - conclude

unit-test:
  image: 
    name: openjdk:17
    pull_policy: if-not-present
  script: ./gradlew test
  stage: unit
  allow_failure: false
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "push"
  artifacts:
    reports:
      junit: build/test-results/test/TEST-*.xml

e2e-test:
  image: registry.gitlab.com/finestructure/pipeline-trigger
  stage: e2e
  allow_failure: true
  script:
    - trigger -h gitlab.cern.ch -a "$API_TOKEN" -p "$CI_JOB_TOKEN" -t dev -e JALIEN_REPO=$CI_REPOSITORY_URL -e JALIEN_BRANCH=$CI_COMMIT_REF_NAME -e TRIGGER_SOURCE="JAliEn" 124456
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "push"
#  variables:
#    JALIEN_REPO: $CI_REPOSITORY_URL
#    JALIEN_BRANCH: $CI_COMMIT_REF_NAME
#  trigger: gitlab.cern.ch/bertranf/jalien-ci
#  script:
#     - 'curl -X POST -F token=${CI_JOB_TOKEN} -F ref="dev" -F "variables[JALIEN_REPO]=$CI_REPOSITORY_URL" -F "variables[JALIEN_BRANCH]=$CI_COMMIT_REF_NAME" https://gitlab.cern.ch/api/v4/projects/124456/trigger/pipeline'

sonarqube-check:
  stage: analysis
  image:
    name: gradle:8.6-jdk17-jammy
    pull_policy: if-not-present
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - gradle sonar
  allow_failure: true
  only:
    - master

sonarqube-vulnerability-report:
  stage: analysis
  script:
    - 'curl -u "${SONAR_TOKEN}:" "${SONAR_HOST_URL}/api/issues/gitlab_sast_export?projectKey=jalien_jalien_78145297-78e8-46b6-8986-f2edbe8f5cce&branch=${CI_COMMIT_BRANCH}&pullRequest=${CI_MERGE_REQUEST_IID}" -o gl-sast-sonar-report.json'
  allow_failure: true
  only:
    - master
  artifacts:
    expire_in: 1 day
    reports:
      sast: gl-sast-sonar-report.json
  dependencies:
    - sonarqube-check

variables:
  OWNER: zensanp
  PIPELINE_REPO: JAliEn-replica-pipeline

call_test_pipeline:
  image: ubuntu:latest
  stage: integration_test
  before_script:
      - apt update
      - apt install -y curl
  script: |
      echo "source_jalien_repo: ${CI_MERGE_REQUEST_SOURCE_PROJECT_URL}, source_jalien_branch:  ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
      curl -v -L \
      -X POST \
      -H "Accept: application/vnd.github+json" \
      -H "Authorization: Bearer ${GH_PAT}" \
      -H "X-GitHub-Api-Version: 2022-11-28" \
      https://api.github.com/repos/${OWNER}/${PIPELINE_REPO}/actions/workflows/99371901/dispatches \
      -d '{"ref":"main", "inputs": { "image_tag": "latest", "source_jalien_repo": "'"${CI_MERGE_REQUEST_SOURCE_PROJECT_URL}"'", "source_jalien_branch":  "'"${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"'"}}'
  rules:
  - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

check_test_output:
  image: ubuntu:latest
  stage: conclude
  before_script:
      - apt update
      - apt install -y curl jq
  script: |
      sleep 60 # sleep for 60 seconds to allow github pipeline to start
      max_iterations=10
      cur_iteration=0

      while [ $cur_iteration -lt $max_iterations ]; do
          cur_iteration=$((cur_iteration + 1))
          workflow_conclusion=$( curl --silent -H "Accept: application/vnd.github+json" \
              -H "Authorization: Bearer ${GH_PAT}" \
              -H "X-GitHub-Api-Version: 2022-11-28" \
              "https://api.github.com/repos/${OWNER}/${PIPELINE_REPO}/actions/runs?per_page=1" | jq '.workflow_runs[0].conclusion' |  sed 's/"//g'
          )

          if [ "$workflow_conclusion" = "success" ]; then
              echo "Test pipeline succesful. Declaring the new changes as stable..."
              break;
          elif [ "$workflow_conclusion" = "failure" ]; then
              echo "Test pipeline failed. Exiting the workflow."
              exit 1
          else
              workflow_status=$( curl --silent -H "Accept: application/vnd.github+json" \
              -H "Authorization: Bearer ${GH_PAT}" \
              -H "X-GitHub-Api-Version: 2022-11-28" \
              "https://api.github.com/repos/${OWNER}/${PIPELINE_REPO}/actions/runs?per_page=1" | jq '.workflow_runs[0].status' |  sed 's/"//g'
              )
              echo "Attempt $cur_iteration/$max_iterations - Current status of the test pipeline is $workflow_status. Retrying in 2 minutes..."
              sleep 120
          fi
      done

      if [ $cur_iteration -eq $max_iterations ]; then
          echo "Failed to pass the test suite. Failing this stage."
          exit 1
      fi
  rules:
  - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
